package time;
import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Kulvi
 * **/
public class TimeTest {


	@Test
		public void testGetTotalSecondsRegular() {
			int totalSeconds = Time.getTotalSeconds("01:01:01");
			assertTrue("The time provided doesnot match the result", totalSeconds == 3661);	
		}

	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0A");
		fail("The time provided is not valid");
	}
	
	@Test
	public void testGetTotalSecondsBoundryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:59");
		assertTrue("The time provided doesnot match the result", totalSeconds == 59);	
	}
	
	@Test
	public void testGetTotalSecondsBoundryOut() {
		int totalSeconds = Time.getTotalSeconds("00:00:60");
		assertTrue("The time provided doesnot match the result", totalSeconds ==  60);	
	}
	
	///////////////////////////////////
	
	@Test
	public void getMillisecondsTestRegular() {
		assertTrue("Invalid time.", Time.getTotalMilliseconds("12:05:05:05") == 5);
	}
	
	
	@Test (expected=NumberFormatException.class)
	public void getMillisecondsTestException() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:0A");
		fail("The time provided is not valid");
	}
	
@Test
	public void getMillisecondsTestBoundryIn() {
		assertTrue("The time is invalid", Time.getTotalMilliseconds("12:05:05:999") == 999);
	}
	
	@Test(expected=NumberFormatException.class)
	public void getMillisecondsTestBoundryOut() {
		assertTrue("The time is invalid", Time.getTotalMilliseconds("12:05:05:1000") != 1000);
	}
	
	
	

}

























//@Test
	/*public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided doesnot match the result", totalSeconds == 3661);	
	}*/
	